use bottomify::bottom::{decode_string, encode_string};
use log::{debug, error};
use teloxide::{
    prelude::*,
    types::{
        InlineQuery,
        InlineQueryResult,
        InlineQueryResultArticle,
        InputMessageContent,
        InputMessageContentText,
        ParseMode,
    },
};
use uuid::Uuid;

#[tokio::main]
async fn main() {
    run().await
}

async fn run() {
    teloxide::enable_logging!();
    let bot = Bot::from_env();
    Dispatcher::new(bot)
        .messages_handler(|rx: DispatcherHandlerRx<Message>| {
            rx.for_each(|cx| async move { handle_message(cx).await })
        })
        .inline_queries_handler(|rx: DispatcherHandlerRx<InlineQuery>| {
            rx.for_each(|cx| async move {handle_inline_query(cx).await})
        })
        .dispatch()
        .await;
}

async fn handle_inline_query(cx: UpdateWithCx<InlineQuery>) {
    let query = cx.update;
    debug!("New inline query: {:?}", query);
    let text = query.query;
    let mut answers = vec![];

    let encoded = encode_string(&text);
    answers.push(
        InlineQueryResultArticle::new(
            Uuid::new_v4().to_string(),
            "Encode text",
            InputMessageContent::Text(InputMessageContentText::new(encoded.to_owned())),
        )
        .description(encoded),
    );

    let res = cx.bot.answer_inline_query(
        query.id,
        answers
            .into_iter()
            .filter(|i| {
                if let InputMessageContent::Text(ref s) = i.input_message_content {
                    s.message_text.len() <= 4096
                } else {
                    true
                }
            })
            .map(InlineQueryResult::Article)
            .collect::<Vec<_>>(),
    );
    debug!("{:?}", res);
    if let Err(e) = res.send().await {
        error!("Failed to answer inline query: {}", e);
    }
}

async fn handle_message(msg: UpdateWithCx<Message>) {
    let text = match msg.update.text() {
        Some(t) => t,
        None => return,
    };
    let dec = decode_string(&text).unwrap_or_else(|err| format!("<b>Error</b>: {}", err));
    if let Err(e) = msg.answer(dec).parse_mode(ParseMode::HTML).send().await {
        error!("Failed to answer message: {}", e);
    }
}
